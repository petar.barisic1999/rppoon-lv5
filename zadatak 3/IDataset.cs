﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace zadatak3
{
	interface IDataset
	{
		ReadOnlyCollection<List<string>> GetData();
	}
}
