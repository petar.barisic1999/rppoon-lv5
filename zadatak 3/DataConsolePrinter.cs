﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak3
{
	class DataConsolePrinter
	{
		public void PrintDataset(IDataset dataset)
		{
			if (dataset.GetData() == null)
			{
				Console.WriteLine("Not allowed");
				return;
			}
			foreach (List<string> stringList in dataset.GetData())
			{
				foreach (string s in stringList)
				{
					Console.Write(s + "\t");
				}
				Console.WriteLine();

			}
			Console.WriteLine();
		}

	}
}
	

