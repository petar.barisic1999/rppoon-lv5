﻿using System;

namespace zadatak3
{
	class Program
	{
		static void Main(string[] args)
		{
			VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset(@"C:\Users\perok\Desktop\Primjer.csv");
			User user1 = User.GenerateUser("User1");
			User user2 = User.GenerateUser("User2");
			ProtectionProxyDataset protectionProxyDataset = new ProtectionProxyDataset(user1);
			ProtectionProxyDataset protectionProxyDataset1 = new ProtectionProxyDataset(user2);
			DataConsolePrinter printer = new DataConsolePrinter();
			Console.WriteLine("Primjer.csv:");
			printer.PrintDataset(virtualProxyDataset);
			Console.WriteLine("Name: " + user1.Name);
			Console.WriteLine("ID: " + user1.ID);
			printer.PrintDataset(protectionProxyDataset);
			Console.WriteLine("Name: " + user2.Name);
			Console.WriteLine("ID: " + user2.ID);
			printer.PrintDataset(protectionProxyDataset1);



		}
	}
}
