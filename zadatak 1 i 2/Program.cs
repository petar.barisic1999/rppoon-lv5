﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadatak1i2
{
	class Program
	{
		static void Main(string[] args)
		{
			Box box = new Box("Proizvodi");
			Product ball = new Product("ball",123,12);
			Product book = new Product("book",100, 32);
			Product shoes = new Product("shoes", 300, 42);
			box.Add(ball);
			box.Add(book);
			box.Add(shoes);
			ShippingService service = new ShippingService(3);
			Console.WriteLine("Total product price: "+ box.Price);
			Console.WriteLine("Total weight: "+ box.Weight);
			//2 zadatak
			Console.WriteLine("Shipping price: "+ service.shippingPrice(box));

		}
	}
}
