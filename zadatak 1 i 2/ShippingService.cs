﻿namespace zadatak1i2
{
	class ShippingService
	{
		private double pricePerWeight;
		public ShippingService(double pricePerWeight)
		{
			this.pricePerWeight = pricePerWeight;
		}
		public double shippingPrice(IShipable item)
		{
			double price = 0;
			price += item.Weight * pricePerWeight;
			return price;
		}
	}
}
